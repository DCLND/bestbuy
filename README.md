# BestBuy MVC
Custom webpack build and testing using mocha and chai.

## Running
Run with `npm run start` available at `http://localhost:8080`

## Tests
Run with `npm run test`