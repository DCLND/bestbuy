// TODO: Testable

/**
 * fetch-jsonp used due to fetch not supporting jsonp format natively currently
 * https://github.com/camsong/fetch-jsonp
 */
import fetchJsonp from 'fetch-jsonp';


export default class CategoriesData {
  /**
   * Creates an instance of Categories.
   * Builds the categories navigation
   *
   * @param {
   * url: String - address of categories
   * container: String - Used for picking the location to inject the results into
   * } options 
   * 
   * @memberof Categories
   */
  constructor(options) {
    const defaults = {
      url: 'http://www.bestbuy.ca/api/v2/json/category/Departments',
    };

    const params = Object.assign({}, defaults, options);
    this.url = params.url;

    this.fetchParams = {
      mode: 'no-cors',
    };
  }

  /**
   * Choreographs getting the JSON response from the BestBuy API
   * using the Fetch API and manages the promise
   *
   * @memberof Categories
   */
  getCategories() {
    // console.log('getting categories');
    return fetchJsonp(this.url, this.fetchParams)
      .then((res) => res.json())
      .then((data) => data.subCategories);
  }
}
