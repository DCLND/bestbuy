export default class CategoriesView {
  /**
   * Creates an instance of Categories.
   * Builds the categories navigation
   *
   * @param {
   * url: String - address of categories
   * container: String - Used for picking the location to inject the results into
   * } options 
   * 
   * @memberof Categories
   */
  constructor(options) {
    const defaults = {
      container: '#categories',
    };

    const params = Object.assign({}, defaults, options);
    this.container = document.querySelector(params.container);
  }

  /**
   * Choreographs splitting out the JSON response, 
   * building the markup and appending to the dom
   * 
   * @param {JSON} data 
   * 
   * @memberof Categories
   */
  loadCategories(categories) {
    /**
     * Builds an array of markup from the
     * array array of categories
     */
    let categoriesMarkupArr = categories.map(category =>
      this.categoryMarkup(category)
    );

    /**
     * Add an 'All' category to the front of the array
     */
    categoriesMarkupArr.unshift(this.categoryMarkup({
      id: '',
      name: 'All'
    }))

    /**
     * Collapse the array to a string with the line breaks
     */
    let categoriesMarkup = categoriesMarkupArr.join('\r\n');

    /**
     * Pass mark-up to be appended
     */
    this.appendCategories(categoriesMarkup);
  }

  /**
   * Returns the mark-up from the single category object
   * 
   * @param {JSON - Category object} category 
   * @returns category anchor mark-up
   * 
   * @memberof Categories
   */
  categoryMarkup(category) {
    const categoryID = category.id;
    const categoryName = category.name;
    return `<a href="/category/${categoryID}" data-category="${categoryID}">${categoryName}</a>`;
  }

  /**
   * Appends the mark-up to the document
   *
   * @param {HTML} markup
   * 
   * @memberof Categories
   */
  appendCategories(markup) {
    this.container.innerHTML = markup;
  }
}