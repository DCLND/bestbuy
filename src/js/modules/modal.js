export default class Modal {
  /**
   * Creates an instance of Modal.
   * @param {
   *   triggers: String - used as a selector for targeting modals
   *   activeClass: String - the active class used when opening modals
   *   modal: HTMLElement - used when calling modal specifically rather than modal listening for triggers
   *   destroyOnClose : Boolean - whether to delete the modal from dom on close 
   * } options 
   * 
   * @memberof Modal
   */
  constructor(options) {
    
    /**
     * Defaults to use for Modal class
     */
    const defaults = {
      triggers: null,
      activeClass: 'active',
      modal: null,
      destroyOnClose: false,
    }
    /**
     * Merging defaults to new object then overwriting with passed options
     */
    const params = Object.assign({}, defaults, options);

    /**
     * setting up variables to be used in this mdoal instance
     */
    this.body = document.documentElement || document.querySelector('body');
    this.activeClass = params.activeClass;
    this.destroyOnClose = params.destroyOnClose;
    this.triggers = params.triggers ? Array.from(document.querySelectorAll(params.trigger)) : null;

    /**
     * Bind the scoping of the events
     * Watch for the trigger clicks if trigger is given
     */
    this.events();

    /**
     * Is a modal was passed, launch the modal
     */
    if (params.modal) {
      this.openModal(params.modal);
    }

  }

  /**
   * Choreographs any event listeners
   * Bind the scope of functions so they can be removed and
   * use the scope of this modal, rather than the event listener target
   * 
   * @memberof Modal
   */
  events() {
    this.triggerHandle = this.triggerHandle.bind(this);
    this.outsideClickHandler = this.outsideClickHandler.bind(this);
    this.closeKeyHandler = this.closeKeyHandler.bind(this);
    this.closeModalHandler = this.closeModal.bind(this);

    if (this.triggers){
      // Checking for if modal has been given triggers to watch
      this.triggers.forEach(trigger => {
        trigger.addEventListener('click', this.triggerHandle, false);
      });
    }
  }

  /**
   * Parses the click event on a modal trigger
   * 
   * @param {Event} e 
   * 
   * @memberof Modal
   */
  triggerHandle(e) {
    e.stopPropagation();
    e.preventDefault();

    const el = e.currentTarget;
    const aim = el.getAttribute('data-modal');

    const target = document.getElementById(aim) ||
      document.querySelector(`[data-modal-target="${aim}"]`);

    target ? this.toggleModal(target) : console.log('no target', target, e);
  }

  /**
   * Used by the triggleHandle if called
   * 
   * @param {any} el 
   * 
   * @memberof Modal
   */
  toggleModal(el) {
    el.classList.contains(this.activeClass) ? this.closeModal() : this.openModal(el);
  }

  /**
   * Choreographs the closing of the modal by
   * running any necessary cleanup functions
   * 
   * @memberof Modal
   */
  closeModal() {
    if (this.modal) {
      this.modal.classList.remove(this.activeClass);
      this.removeListeners();
      if(this.destroyOnClose){
        this.destroyModal();
      }
      this.modal = null;
    } else {
      console.log('no modal to close');
    }
  }

  /**
   * Choreographs the opening of a modal
   * 
   * @param {HTMLElement} el 
   * 
   * @memberof Modal
   */
  openModal(el) {
    this.setModalDom(el);
    this.modal.classList.add(this.activeClass);
    this.listen();
  }

  /**
   * Sets up the active modal parts to be consumed by the modal class instance
   * 
   * @param {HTMLElement} el 
   * 
   * @memberof Modal
   */
  setModalDom(el) {
    this.modal = el;
    this.modalContainer = el.querySelector('.modal__container');
    this.modalContent = el.querySelector('.modal__content');
    this.modalClose = Array.from(el.querySelectorAll('[data-modal-close]'));
  }

  /**
   * Removes the modal from the DOM
   * 
   * 
   * @memberof Modal
   */
  destroyModal() {
    // console.log('destroying modal');
    this.modal.parentNode.removeChild(this.modal);
  }

  /**
   * Cleans up the listeners applied by listen()
   * 
   * 
   * @memberof Modal
   */
  removeListeners() {
    if (this.listening) {
      this.body.removeEventListener('click', this.outsideClickHandler);
      this.body.removeEventListener('keydown', this.closeKeyHandler);
      this.listening = false;
    }
  }

  /**
   * Listens for events to manage closing the modal
   * 
   * 
   * @memberof Modal
   */
  listen() {
    this.body.addEventListener('click', this.outsideClickHandler, false);
    this.body.addEventListener('keydown', this.closeKeyHandler, false);
    this.modalClose.forEach(close =>
      close.addEventListener('click', this.closeModalHandler)
      );
    this.listening = true;
  }

  /**
   * Checks for the ESC key
   * 
   * @param {event} e 
   * 
   * @memberof Modal
   */
  closeKeyHandler(e) {
    if (this.modal && e.which === 27) {
      e.preventDefault();
      this.closeModal();
    }
  }

  /**
   * Tracks if the click was inside the modalContainer
   * If inside modalContainer - continue as normal
   * It outside the modalContainer - close the modal
   * 
   * @param {Event} e 
   * @returns Void
   * 
   * @memberof Modal
   */
  outsideClickHandler(e) {
    let node = e.target;
    while (node && node !== this.body) {
      if (node === this.modalContainer) {
        return;
      }
      node = node.parentNode;
    }
    this.closeModal();
  }

}
