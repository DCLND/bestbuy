import 'whatwg-fetch';

// Works for node test environment but fails CORS in browser
import fetch from 'isomorphic-fetch';

// Using JSONP to padding the request to bypass CORS issues
// Fails in tests due to no fetch method
import fetchJsonp from 'fetch-jsonp';

// http://www.bestbuy.ca/api/v2/json/search?categoryid=20001
// http://www.bestbuy.ca/api/v2/json/search?categoryid=departments

/**
 * Handles the products related data for the app
 * 
 * @export
 * @class ProductsData
 */
export default class ProductsData {
  /**
   * Creates an instance of ProductsData.
   * @param {urlBase: String - API urlbase to query against} options 
   * 
   * @memberof ProductsData
   */
  constructor(options) {
    /**
     * defining defaults to use if no options passed
     */
    const defaults = {
      urlBase: 'http://www.bestbuy.ca/api/v2/json/search?categoryid=',
    };

    const params = Object.assign({}, defaults, options);

    this.urlBase = params.urlBase;
    this.fetchParams = {
      mode: 'no-cors',
    };
  }

  /**
   * Fetches products and parses out the products from
   * the response. Returns the fetch promise to allow orchestration
   * elsewhere
   * 
   * @param {String||Integer} id 
   * @returns Fetch Promise with parsed JSON
   * 
   * @memberof ProductsData
   */
  getProducts(id) {
    const fetchUrl = `${this.urlBase}${id}`;

    return fetchJsonp(fetchUrl, this.fetchParams)
      .then((res) => res.json())
      .then((data) => data.products);
  }

  /**
   * Gets a single product
   * 
   * @param {any} id 
   * @returns Fetch Promise with parsed JSON
   * 
   * @memberof ProductsData
   */
  getProduct(id) {
    const fetchUrl = `${this.urlBase}${id}`;
    return fetchJsonp(fetchUrl, this.fetchParams)
      .then((res) => res.json())
      .then((data) => data);
  }
}
