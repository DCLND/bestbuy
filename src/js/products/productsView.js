import Modal from '../modules/modal';

/**
 * Handles the products view functionality
 *
 * @export
 * @class ProductsView
 */
export default class ProductsView {
  /**
   * Only parses the container item.
   * @param {any} options
   *
   * @memberof ProductsView
   */
  constructor(options) {
    /**
     * Defaults to be used
     */
    const defaults = {
      container: '#main-view',
    };

    const params = Object.assign({}, defaults, options);

    this.container = document.querySelector(params.container);
    if (!this.container) {
      throw new Error(`could not find ${params.container}`);
    }
  }

  /**
   * Load the products JSON array into the DOM
   * 
   * @param {any} products
   * 
   * @memberof ProductsView
   */
  loadProducts(products) {
    let productsMarkupArr = products.map((product) =>
      this.buildProductMarkup(product)
    );
    let productsMarkup = productsMarkupArr.join('\r\n');

    this.appendProducts(productsMarkup);
  }

  /**
   * Builds the product markup
   *
   * @param {any} product
   * @returns String - Template
   *
   * @memberof ProductsView
   */
  buildProductMarkup(product) {
    /**
     * ES6 destructuriing the product item
     */
    const {
      name,
      regularPrice,
      sku,
      salePrice,
      thumbnailImage,
      hideSavings,
    } = product;

    /**
     * Using an ES6 template literal as a template engine
     * for simplicity
     */
    return `<a href="" class="product" data-product="${sku}">
      <h2 class="product__name">${name}</h2>
      <img src="${thumbnailImage}" />
      <div class="product__prices">Price: ${hideSavings ? `` : `<s>${regularPrice}</s>`} ${salePrice}</div>
    </a>`;
  }

  /**
   * Appends the markup to the instance's container
   *
   * @param {String - Template string} markup
   *
   * @memberof ProductsView
   */
  appendProducts(markup) {
    this.container.innerHTML = markup;
  }

  /**
 * Takes the JSON product response and composes the functions for building
 * the mark-up.
 *
 * @param {JSON Product info} product
 *
 * @memberof Product
 */
  launchProduct(product) {
    // console.log('launch Product:', product);
    this.modalId = product.sku;
    const productMarkup = this.buildProductModalMarkup(product);
    const modal = this.buildModalMarkup(productMarkup, product.sku);
    this.appendModal(modal);
  }

  /**
   * Builds out the markup for the product by destructuring the product
   * JSON and writing the template using template literals rather than relying
   * on another library for simplicity
   * 
   * @param {JSON Product} product
   * @returns product html markup
   * 
   * @memberof Product
   */
  buildProductModalMarkup(product) {
    const {
      name,
      regularPrice,
      productUrl,
      sku,
      shortDescription,
      salePrice,
      thumbnailImage,
      hideSavings,
    } = product;

    return `<div class="product-modal" id="${sku}">
      <h1 class="product-modal__name">${name}</h1>
      <img src="${thumbnailImage}" />
      <p class="product-modal__description">${shortDescription}</p>
      <div class="product-modal__prices">Price: ${hideSavings ? `` : `<s>${regularPrice}</s>`} ${salePrice}</div>
      <a href="${productUrl}" target="_blank">Visit Product <i class="material-icons">open_in_new</i></a>
    </div>`;
  }

  /**
   * Builds out the HTML for the modal to launch rather than
   * extending the modal
   * @param {HTML String} productMarkup
   * @returns modal html
   *
   * @memberof Product
   */
  buildModalMarkup(productMarkup, productId) {
    let modal = document.createElement('DIV');
    modal.id = productId;
    modal.className = 'modal';

    let modalContainer = document.createElement('DIV');
    modalContainer.className = 'modal__container';
    modalContainer.innerHTML = productMarkup;

    let modalClose = document.createElement('BUTTON');
    modalClose.className = 'modal__close';
    modalClose.innerHTML = 'Close X';
    modal.appendChild(modalClose);

    modal.appendChild(modalContainer);

    return modal;
  }

  /**
   * Appends the modal to the body
   *
   * @param {HTML Object} modal
   *
   * @memberof Product
   */
  appendModal(modal) {
    this.container.appendChild(modal);
    this.launchProductModal();
  }

  /**
   * Get the DOM reference of the appended modal html and
   * then makes a new Modal instance passing over the HTML Object
   *
   *
   * @memberof Product
   */
  launchProductModal() {
    const modal = document.getElementById(this.modalId);

    new Modal({
      modal: modal,
      destroyOnClose: true,
    });
  }
}
