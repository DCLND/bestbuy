// Main app file
// ===========================================
// TODO: Eslint
// ===========================================


// ===========================================
// URLS
// http://www.bestbuy.ca/api/v2/json/category/Departments
// http://www.bestbuy.ca/api/v2/json/search?categoryid=20001
// http://www.bestbuy.ca/api/v2/json/search?categoryid=departments
// http://www.bestbuy.ca/api/v2/json/product/10381162
// ===========================================


/**
 * Fetch promise polyfill
 * https://github.com/github/fetch
 */
import 'whatwg-fetch';

/**
 * Simple style import for parsing with webpack
 */
import '../css/styles.sass';

// Dependencies
// import Categories from './modules/categories';
import CategoriesData from './categories/categoriesData';
import CategoriesView from './categories/categoriesView';
// import Products from './modules/products';
import ProductsData from './products/productsData';
import ProductsView from './products/productsView';

/**
 * BestBuy MVC App
 *
 * @class BestBuy
 */
class BestBuy {
  /**
   * Launches the BestBuy class that orchestrates the app
   *
   * @memberof BestBuy
   */
  constructor() {
    this.body = document.documentElement || document.querySelector('body');

    this.productsData = new ProductsData({
      urlBase: 'http://www.bestbuy.ca/api/v2/json/search?categoryid=',
    });

    this.singleProductData = new ProductsData({
      urlBase: 'http://www.bestbuy.ca/api/v2/json/product/',
    });

    this.productsView = new ProductsView({
      container: '#main-view',
    });

    this.categoriesData = new CategoriesData();
    this.categoriesView = new CategoriesView();

    this.initView();

    this.events();
  }

  /**
   * Handle the global click event
   *
   * @memberof BestBuy
   */
  events() {
    // Watch the body and parse events that bubble to the top of the DOM
    this.parseClickEvent = this.parseClickEvent.bind(this);
    this.body.addEventListener('click', this.parseClickEvent);
  }

  /**
   * Initialise by populating the views with data
   *
   * @memberof BestBuy
   */
  initView() {
    /**
     * Get all the categories and post them to the categoriesView
     */
    this.categoriesData.getCategories()
      .then((categories) => this.categoriesView.loadCategories(categories))
      .catch((err) => this.errorHandler(err));

      /**
       * Get initial product data and load into view
       */
    this.productsData.getProducts('departments')
      .then((data) => this.productsView.loadProducts(data))
      .catch((err) => this.errorHandler(err));
  }

  /**
   * Used to parse and orchestrate all the relevant actions for the classes
   * Parsing attributes for the available actions with data-*
   * Simple method to make elements more extensible through attributes
   * 
   * @param {Event} event
   * @returns Void
   * 
   * @memberof BestBuy
   */
  parseClickEvent(event) {
    // console.log('Parse Click Event', event, event.target);
    let target = event.target || event.srcElement;

    /**
     * Incase clicked an element inside of an anchor
     * Loop through parents until an instance of HTMLAnchorElement
     * is found.
     */
    while (target) {
      if (target instanceof HTMLAnchorElement) {
        target = target;
        break;
      }
      target = target.parentNode;
    }
    /**
     * If no HTMLAnchorElement found, carry on
     */
    if (!target) return true;

    /**
     * If a dataset is found, parse the possible actions
     */
    const dataset = target.dataset;
    if (dataset) {
      this.parseAction(event, dataset);
    }
  }

  /**
   * Loop through the dataset object and launch available actions
   *
   * @param {Event} event
   * @param {String - HTML element attributes} dataset
   *
   * @memberof BestBuy
   */
  parseAction(event, dataset) {
    for (let action in dataset) {
      if (dataset.hasOwnProperty(action)) {
        let content = dataset[action];
        switch (action) {
          case 'category':
            console.log('get category', content);
            this.productsData.getProducts(content)
              .then((data) => this.productsView.loadProducts(data))
              .catch((err) => this.errorHandler(err));
            break;
          case 'product':
            console.log('get product', content);
            this.singleProductData.getProduct(content)
              .then((product) => this.productsView.launchProduct(product))
              .catch((err) => this.errorHandler(err));
            // new Product(content);
            break;
          default:
            console.log('no action found');
            return;
            // break;
        }

        // Stop default as action taking place if dataset has action property
        event.preventDefault();
      }
    }
  }

  /**
   * Simple error handling
   *
   * @param {Error} err
   *
   * @memberof BestBuy
   */
  errorHandler(err) {
    throw new Error(err);
    // console.log('an error: ', err);
  }
}

/**
 * Initiates the BestBuy app
 * Use on the window for viewability
 */
window.BestBuy = new BestBuy();
