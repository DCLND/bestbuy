// BABEL destructoring not working

const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = env => {
  return {
    // context: path.resolve(__dirname, './src'),
    entry: './src/js/app.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, './dist/assets/'),
      publicPath: '/assets',
      // pathinfo: !env.prod
    },
    devServer: {
      contentBase: path.resolve(__dirname, 'src'),
    },
    devtool: env.prod ? 'source-map' : 'eval',
    module: {
    rules: [
        {
          test: /\.js$/,
          exclude: [/\node_modules/],
          use: [{
            loader: 'babel-loader',
            options: {
              presets: ['latest'],
            },
          }],
        },
        {
          test: /\.(sass|scss)$/,
          // use: ['style-loader', 'css-loader', 'sass-loader']
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader']
          })
        }
      ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'style.css'
        })
    ]
  }
}