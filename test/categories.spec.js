// TODO: Full test coverage
// TODO: Remove global dom pollution, needed for externals to have access to the window and document

import * as chai from 'chai';
import CategoriesData from '../src/js/categories/categoriesData';
import CategoriesView from '../src/js/categories/categoriesView';
import {JSDOM} from 'jsdom';

const {expect} = chai;


describe('Categories', function() {
  beforeEach(function() {
    const dom = new JSDOM('<!DOCTYPE html><body><div id="main-view"></div></body>');
    global.window = dom.window;
    global.document = dom.window.document;
  });

  describe('CategoriesData', function() {
    it('Testing defaults', function() {
      const categoriesData = new CategoriesData();
      expect(categoriesData.url).to.be.a('string');
      expect(categoriesData.url).to.equal('http://www.bestbuy.ca/api/v2/json/category/Departments');
    });

    // Leaving due to node fetch API issues. Works either in node or browser,
    // Passes with a node enabled fetch like isomorphic-fetch or node-fetch
    // but they fail in the browser
    // it('getProducts returning array', function() {
    //   const catgoriesData = new catgoriesData();

    //   // Passes no matter what
    //   return catgoriesData.getCategories()
    //     .then((res) => {
    //       expect(res).to.be.an('array');
    //     });
    // });
  });
});
