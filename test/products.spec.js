// TODO: Full test coverage

import * as chai from 'chai';
import ProductsData from '../src/js/products/productsData';
import ProductsView from '../src/js/products/productsView';
import {JSDOM} from 'jsdom';
import chaiAsPromised from 'chai-as-promised';
import chaiDom from 'chai-dom';

const {expect} = chai;

// To allow use of .eventually
chai.use(chaiAsPromised);
// chai.use(chaiDom);

const demoProduct = {
  "sku": "10586898",
  "name": "Lenovo Tab 10.1\" 16GB Android 6.0 Tablet w/ Qualcomm Snapdragon Quad-Core Processor-Slate Black",
  "regularPrice": 169.95,
  "salePrice": 169.95,
  "thumbnailImage": "https://multimedia.bbycastatic.ca/multimedia/products/150x150/105/10586/10586898.jpg",
  "productUrl": "/en-ca/product/lenovo-lenovo-tab-10-1-16gb-android-6-0-tablet-w-qualcomm-snapdragon-quad-core-processor-slate-black-za1u0003us/10586898.aspx?",
  "hideSavings": true,
  "shortDescription": "Entertainment is at your fingertips with the Lenovo Tab 10. Running on Android 6.0 Marshmallow, this 10.1\" tablet boasts a 10-hour battery life and dual stereo speakers with Dolby Digital Plus audio. Stream movies, play music, explore apps and more through the tablet's 1280 x 800 resolution touchscreen."
};

describe('Products', function() {
  beforeEach(function() {
    const dom = new JSDOM('<body><div id="main-view"></div></body>');
    window = dom.window;
    document = dom.window.document;
  });

  describe('ProductsData', function() {
    it('Testing defaults', function() {
      const productsData = new ProductsData();
      expect(productsData.urlBase).to.be.a('string');
      expect(productsData.urlBase).to.equal('http://www.bestbuy.ca/api/v2/json/search?categoryid=');
    });

    it('ProductData taking new urlBase', function() {
      const productsData = new ProductsData({
        urlBase: 'http://www.bestbuy.ca/api/v2/json/product/',
      });
      expect(productsData.urlBase).to.equal('http://www.bestbuy.ca/api/v2/json/product/');
    });

    // Leaving due to node fetch API issues. Works either in node or browser,
    // Passes with a node enabled fetch like isomorphic-fetch or node-fetch
    // but they fail in the browser
    // it('getProducts returning array', function() {
    //   const productsData = new ProductsData();

    //   // Passes no matter what
    //   return productsData.getProducts('departments')
    //     .then((res) => {
    //       expect(res).to.be.an('array');
    //     });
    // });
  });

  describe('ProductsView', function() {
    it('container default returns a HTMLDivElement', function() {
      const productsView = new ProductsView();
      expect(productsView.container).to.be.a('HTMLDivElement');
    });

    it('invalid container error check', () => {
      expect(() => new ProductsView({
        container: '#foo',
      })).to.throw('could not find #foo');
    });

    it('buildProductMarkUp return string', function() {
      const productsView = new ProductsView();

      expect(productsView.buildProductMarkup(demoProduct)).to.be.a('string');
    });

    it('loadProducts appends anchor to container', function() {
      const productsView = new ProductsView();

      const products = [
        demoProduct,
        demoProduct,
        demoProduct,
      ];

      productsView.loadProducts(products);
      expect(document.querySelector('.product')).to.be.a('HTMLAnchorElement');
    });

    it('launchProduct appends modal to container', function() {
      const productsView = new ProductsView();

      productsView.launchProduct(demoProduct);
      expect(document.querySelector('.modal').id).to.be.equal(demoProduct.sku);
    });
  });
});
