// TODO: Full test coverage

import * as chai from 'chai';
import Modal from '../src/js/modules/modal';
import { JSDOM } from 'jsdom';
import chaiDom from 'chai-dom';

const { expect } = chai;

chai.use(chaiDom);


describe('Modal', function() {
    beforeEach(() => {
      const dom = new JSDOM(`<body>
        <div class="modal">
          <button data-modal-close></button>
          <div class="modal__container"></div>
        </div></body>`);
      window = dom.window;
      document = dom.window.document;
    });

  it('triggers launch modal', () => {
  });

  it('applying activeClass if passed HTMLElement', () => {
    const modalDom = document.querySelector('.modal');
    const modal = new Modal({
      modal: modalDom,
    });
    expect(modalDom.classList.contains('active')).to.true;
  });

  it('click modal__close closes modal', () => {
    const modalDom = document.querySelector('.modal');
    const modalClose = document.querySelector('[data-modal-close]');
    const modal = new Modal({
      modal: modalDom,
    });

    const evt = document.createEvent('HTMLEvents');
    evt.initEvent('click', false, true);
    modalClose.dispatchEvent(evt);

    expect(modalDom.classList.contains('active')).to.be.false;
  });

  it('removing from dom if destroy enabled', () => {
    const modalDom = document.querySelector('.modal');
    const modal = new Modal({
      modal: modalDom,
      destroyOnClose: true,
    });

    modal.closeModal();

    expect(document.querySelector('.modal')).to.be.null;
  });
});
